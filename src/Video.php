<?php

namespace YourMediaKit;

use YourMediaKit\Traits\ApiTrait;
use YourMediaKit\Traits\HttpClientTrait;

/**
 * YourMediaKit Video API Consumer Class
 *
 * @package YourMediaKit
 */
class Video
{
	use ApiTrait;
	use HttpClientTrait;

	/**
	 * Video constructor.
	 *
	 * @param [] $config
	 */
	public function __construct($config)
	{
		if (!empty($config['apiKey'])) {
			$this->setApiKey($config['apiKey']);
		} else {
			throw new \Error('Missing API key in YourMediaKit\Video instantization');
		}

		if (!empty($config['apiSecret'])) {
			$this->setApiSecret($config['apiSecret']);
		} else {
			throw new \Error('Missing API secret in YourMediaKit\Video instantization');
		}

		if (!empty($config['apiHost'])) {
			$this->setApiHost($config['apiHost']);
		} else {
			$this->setApiHost($this->getDefaultApiHost());
		}

		$this->setHttpClient(['base_uri' => $this->getApiHost()]);
	}

	/**
	 * Fetch a signed upload URL for the video file
	 *
	 * @param array $params
	 *
	 * @return array|string
	 */
	public function getUploadUrl(array $params)
	{
		try {
			$response = $this->getHttpClient()->get(
				$this->getApiHost() . '/videos/get-upload-url',
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret(),
						'filename' => $params['filename'],
						'contentType' => $params['contentType'],
					]
				]
			);

			if ($response->getStatusCode() == 200) {
				return json_decode((string)$response->getBody(), true)['url'];
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Create video entity call
	 *
	 * @param array $params
	 *
	 * @return int|string
	 */
	public function createEntity(array $params)
	{
		try {
			$response = $this->getHttpClient()->post(
				$this->getApiHost() . '/videos/create-entity',
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret()
					],
					'form_params' => $params
				]
			);

			if ($response->getStatusCode() == 200) {
				return (int)json_decode((string)$response->getBody(), true)['entityId'];
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Upload source video to CORS bucket
	 *
	 * @param $src
	 * @param $dest
	 *
	 * @return bool|null
	 */
	public function uploadSourceToBucket($src, $dest)
	{
		try {
			$body = fopen($src, 'r');

			if ($body !== false) {
				$response = $this->getHttpClient()->put(
					$dest,
					['body' => fopen($src, 'r')]
				);

				if ($response->getStatusCode() == 200) {
					return true;
				}
			}
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Request video import job to start for given video id and video file source object
	 *
	 * @param int    $entityId
	 * @param string $sourceObjectFilename
	 *
	 * @return array|string
	 */
	public function startImportJob(int $entityId, string $sourceObjectFilename)
	{
		try {
			$response = $this->getHttpClient()->get(
				$this->getApiHost() . '/videos/start-import-job',
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret(),
						'entityId' => $entityId,
						'objectFilename' => $sourceObjectFilename
					]
				]
			);

			if ($response->getStatusCode() == 200) {
				return json_decode((string)$response->getBody(), true)['jobId'];
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Fetch job info
	 *
	 * @param $jobId
	 *
	 * @return array|string
	 */
	public function getJobStatus($jobId)
	{
		try {
			$response = $this->getHttpClient()->get(
				$this->getApiHost() . '/videos/get-job-status/' . $jobId,
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret()
					]
				]
			);
			if ($response->getStatusCode() == 200) {
				return json_decode((string)$response->getBody(), true);
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}
}
