<?php

namespace YourMediaKit;

use YourMediaKit\Traits\ApiTrait;
use YourMediaKit\Traits\HttpClientTrait;

class VideoEmbed
{
	use ApiTrait;
	use HttpClientTrait;

	/**
	 * VideoEmbed constructor.
	 *
	 * @param [] $config
	 */
	public function __construct($config)
	{
		if (!empty($config['apiKey'])) {
			$this->setApiKey($config['apiKey']);
		} else {
			throw new \Error('Missing API key in YourMediaKit\VideoEmbed instantization');
		}

		if (!empty($config['apiSecret'])) {
			$this->setApiSecret($config['apiSecret']);
		} else {
			throw new \Error('Missing API secret in YourMediaKit\VideoEmbed instantization');
		}

		if (!empty($config['apiHost'])) {
			$this->setApiHost($config['apiHost']);
		} else {
			$this->setApiHost($this->getDefaultApiHost());
		}

		$this->setHttpClient(['base_uri' => $this->getApiHost()]);
	}

	/**
	 * View an embed for a video
	 *
	 * @param int  $id the id of the embed (not the video)
	 *
	 * @param bool $onlyEmbedString
	 *
	 * @return array|string
	 */
	public function view($id, $onlyEmbedString = true)
	{
		try {
			$response = $this->getHttpClient()->get(
				$this->getApiHost() . '/video-embeds/view/' . $id,
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret()
					]
				]
			);

			if ($response->getStatusCode() == 200) {
				$responseData = json_decode((string)$response->getBody(), true);
				if ($onlyEmbedString) {
					return $responseData['embedString'];
				}
				return $responseData;
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Create an embed for a video
	 *
	 * @param array $params
	 *
	 * @return array|string
	 */
	public function add($params)
	{
		try {
			$response = $this->getHttpClient()->put(
				$this->getApiHost() . '/video-embeds/add',
				[
					'query' => [
						'apiKey' => $this->getApiKey(),
						'apiSecret' => $this->getApiSecret()
					],
					'form_params' => $params
				]
			);

			if ($response->getStatusCode() == 200) {
				return json_decode((string)$response->getBody(), true);
			} elseif ($response->getStatusCode() == 409) {
				return json_decode((string)$response->getBody(), true)['error'];
			}

			return (string)$response->getBody();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}
}
