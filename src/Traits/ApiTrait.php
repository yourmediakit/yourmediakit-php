<?php

namespace YourMediaKit\Traits;

trait ApiTrait
{
	/**
	 * API Key
	 *
	 * @var string
	 */
	private $apiKey;

	/**
	 * API Secret
	 *
	 * @var string
	 */
	private $apiSecret;

	/**
	 * YMK API host
	 *
	 * @var string
	 */
	private $apiHost;


	/**
	 * Get API Key
	 *
	 * @return string
	 */
	private function getApiKey(): string
	{
		return $this->apiKey;
	}

	/**
	 * Set API Key
	 *
	 * @param string $apiKey
	 */
	private function setApiKey(string $apiKey): void
	{
		$this->apiKey = $apiKey;
	}

	/**
	 * Get API Secret
	 *
	 * @return string
	 */
	private function getApiSecret(): string
	{
		return $this->apiSecret;
	}

	/**
	 * Set API Secret
	 *
	 * @param string $apiSecret
	 */
	private function setApiSecret(string $apiSecret): void
	{
		$this->apiSecret = $apiSecret;
	}

	/**
	 * Get API host
	 *
	 * @return string
	 */
	public function getApiHost(): string
	{
		return $this->apiHost;
	}

	/**
	 * Get default API host
	 *
	 * @return string
	 */
	public function getDefaultApiHost(): string
	{
		return 'https://yourmediakit.com/api';
	}

	/**
	 * Set API host
	 *
	 * @param string $apiHost
	 */
	public function setApiHost(string $apiHost): void
	{
		$this->apiHost = $apiHost;
	}
}
