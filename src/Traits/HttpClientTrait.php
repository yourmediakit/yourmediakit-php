<?php

namespace YourMediaKit\Traits;

use GuzzleHttp\Client;

trait HttpClientTrait
{
	/**
	 * HTTP client
	 *
	 * @var Client
	 */
	private $httpClient;


	/**
	 * Get API Client
	 *
	 * @return Client
	 */
	private function getHttpClient(): Client
	{
		return $this->httpClient;
	}

	/**
	 * Set API Client
	 *
	 * @param array $config
	 */
	private function setHttpClient(array $config): void
	{
		$this->httpClient = new Client($config);
	}
}
